/* A Simple HTTP Server. */

/*********************************************************
 * Authors information
 *
 * === User information ===
 * User 1: arnara13
 * SSN: 0608893339
 * User 2: gunnarmar13
 * SSN: 2609835209
 * User 3: asgeirt13
 * SSN: 2810933209
 * === End User Information ===
 ********************************************************/

#include <assert.h>
#include <sys/select.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <stdio.h>
#include <glib.h>
#include <stdlib.h>

void getTimeStamp();    // This function prints timestamp to log file 
char *getWebPage();     // Prints out the requestURL
char *getPayload();     // Returns the payload for post function
void getwebsite();		// Generates webpage with or withoud payload
char *getColor();		// Returns color argument
void getColorWebsite();	// Returns a webpage with background color
char *getRequestUrl();  // Returns the Request URL
char *getCookieValue();	// Returns value from cookie if available.

int main(int argc, char **argv)
{
        int sockfd;
        struct sockaddr_in server, client;
        char message[512];
        int port;

        /* Create and bind a UDP socket */
        sockfd = socket(AF_INET, SOCK_STREAM, 0);
        memset(&server, 0, sizeof(server));
        server.sin_family = AF_INET;
        /* Network functions need arguments in network byte order instead of
           host byte order. The macros htonl, htons convert the values, */
        server.sin_addr.s_addr = htonl(INADDR_ANY);
        port = atoi(argv[1]);
        server.sin_port = htons(port);
        bind(sockfd, (struct sockaddr *) &server, (socklen_t) sizeof(server));

    /* Before we can accept messages, we have to listen to the port. We allow one
     * 1 connection to queue for simplicity.
     */
    listen(sockfd, 1);

        for (;;) {
                fd_set rfds;
                struct timeval tv;
                int retval;

                /* Check whether there is data on the socket fd. */
                FD_ZERO(&rfds);
                FD_SET(sockfd, &rfds);

                /* Wait for five seconds. */
                tv.tv_sec = 5;
                tv.tv_usec = 0;
                retval = select(sockfd + 1, &rfds, NULL, NULL, &tv);

                if (retval == -1) {
                        perror("select()");
                } else if (retval > 0) {
                        /* Data is available, receive it. */
                        assert(FD_ISSET(sockfd, &rfds));

                        /* Copy to len, since recvfrom may change it. */
                        socklen_t len = (socklen_t) sizeof(client);

                        /* For TCP connectios, we first have to accept. */
                        int connfd;
                        connfd = accept(sockfd, (struct sockaddr *) &client,
                                        &len);
                        
                        /* Receive one byte less than declared,
                           because it will be zero-termianted
                           below. */
                        ssize_t n = read(connfd, message, sizeof(message) - 1);

                        /* Check types of request from message */
                        gboolean isGet = g_str_has_prefix(message, "GET");
                        gboolean isPost = g_str_has_prefix(message, "POST");
                        gboolean isHead = g_str_has_prefix(message, "HEAD");

                        char url_string[strlen(message)];
                        strcpy(url_string, message);

                        /* Get clientPort */
                        int portno = ntohs(client.sin_port);

                        /* New GString */
                        GString * response = g_string_new(NULL);
                        //GString * html = g_string_new(NULL);
                        
                        /* Open connection for log file with append */
                        FILE * fPointer;
                        fPointer = fopen("httpd.log", "a");

                        /* Retrieve ip address of client and cast to string */
                        int ipAddr = client.sin_addr.s_addr;
                        char client_ip[INET_ADDRSTRLEN];
                        inet_ntop( AF_INET, &ipAddr, client_ip, INET_ADDRSTRLEN );

                        /* Retrieve the request url */

                        char requestUrl[strlen(message)];
                        strcpy(requestUrl, getRequestUrl(url_string));

                        gboolean rawpage = g_str_has_prefix(requestUrl, "/");
                        gboolean colorassign = g_str_has_prefix(requestUrl, "/color?bg=");
                        gboolean colorcookie = g_str_has_prefix(requestUrl, "/color");

                        /* retrieve host url */
                        char messagecpy[strlen(message)];
                        strcpy(messagecpy, message);

                        char host[1000];
                        strcpy(host, getWebPage(messagecpy));
                        strcat(host,requestUrl);

                        /* Getting payload for post func */
                        char payloadstring[strlen(message)];
                        strcpy(payloadstring, message);

                        char payload[strlen(message)];
                        strcpy(payload, getPayload(payloadstring));

                        char colorPayloadstring[strlen(message)];
                        strcpy(colorPayloadstring, message);
                        char colorPayload[strlen(message)];
                        strcpy(colorPayload, getCookieValue(colorPayloadstring));

                        char color[strlen(message)];
                        strcpy(color, message);
                        strcpy(color, getColor(color));

                        if (isGet)
                        {
                        	/* Header of response */
                            g_string_append(response,"HTTP/1.1 200 OK\r\n");
                            g_string_append(response,"Content-Type: text/html\r\n");
                            g_string_append(response,"Client port: ");
                            g_string_append(response, g_strdup_printf("%i \r\n", portno));
                            g_string_append(response,"Request URL: ");
                            g_string_append(response, g_strdup_printf("%s \r\n", host));
                            g_string_append(response, "Remote Address: ");
                            g_string_append(response, g_strdup_printf("%s \r\n", client_ip));

                            /* if the request has prefix "*.* /color?bg="*/
                            if(colorassign) {
                                g_string_append(response,"Set-Cookie: cValue=");
                                g_string_append(response, g_strdup_printf("%s \r\n", color));
                                g_string_append(response, "\r\n");
                                g_string_append(response,"\r\n");
                                getColorWebsite(response, color);

                            }
                            /* if the request has prefix "*.* /color" */
                           	else if(colorcookie) {
                                g_string_append(response, "\r\n");
                                g_string_append(response,"\r\n");
                            	getColorWebsite(response, colorPayload);
                            }
                            /* if the request has prefix "*.* /"" */
                            else if (rawpage) {
                                g_string_append(response, "\r\n");
                               	g_string_append(response,"\r\n");
                                getwebsite(response, payload);
                            }



                            /* writes single line of info to httpd.log */
                            getTimeStamp();
                            fprintf(fPointer, ": %s:%s %s %s : %s \n", client_ip, g_strdup_printf("%i", portno),
                                    "GET", g_strdup_printf("%s", host), "200");
                            
                        }
                        if (isPost)
                        {
                            getwebsite(response, payload);

                            /* writes single line of info to httpd.log */
                            getTimeStamp();
                            fprintf(fPointer, ": %s:%s %s %s : %s \n", client_ip, g_strdup_printf("%i", portno),
                                    "POST", g_strdup_printf("%s", host), "201");

                        }
                        if (isHead)
                        {
                            /* Header of response */
                            g_string_append(response,"HTTP/1.1 200 OK\r\n");
                            g_string_append(response,"Content-Type: text/html\r\n");
                            g_string_append(response,"Client port: ");
                            g_string_append(response, g_strdup_printf("%i \r\n", portno));
                            g_string_append(response,"Request URL: ");
                            g_string_append(response, g_strdup_printf("%s \r\n", host));
                            g_string_append(response, "Remote Address: ");
                            g_string_append(response, g_strdup_printf("%s \r\n", client_ip));

                            /* writes single line of info to httpd.log */
                            getTimeStamp();
                            fprintf(fPointer, ": %s:%s %s %s : %d\n", client_ip, g_strdup_printf("%i", portno),
                                    "HEAD", host, 200);
                        }
                        
                        /* Send the message back. */
                        write(connfd, response->str, response->len);

                        //g_string_free(html,TRUE);
                        g_string_free(response,TRUE);

                        /* Closing filePointer */
                        fflush(fPointer);
                        fclose(fPointer);

                        /* We should close the connection. */
                        shutdown(connfd, SHUT_RDWR);
                        close(connfd);

                        /* Zero terminate the message, otherwise
                           printf may access memory outside of the
                           string. */
                        message[n] = '\0';
                        /* Print the message to stdout and flush. */
                        fprintf(stdout, "Received:\n%s\n", message);
                        fflush(stdout);
                } else {
                        fprintf(stdout, "No message in five seconds.\n");
                        fflush(stdout);
                }
        }
}

/* This function prints timestamp to log file */
void getTimeStamp() {
    FILE * fPointer;
    fPointer = fopen("httpd.log", "a");

    struct tm *local;
    time_t t;
    t = time(NULL);
    local = localtime(&t);
    fprintf(fPointer, "%d-%d-%dT%d:%d:%d ", local->tm_year+1900,local->tm_mon+1,local->tm_mday, local->tm_hour,local->tm_min,local->tm_sec);
    fclose(fPointer);
}

char *getWebPage(char message[]) {
    const char CRLF[5] = " \r\n";
    const char getter[6] = "Host:";
    char *token;
    int ret;

    /* New pointer for returnValue*/
    char *url = malloc (sizeof (char) * 20);

    /* Get the first token */
    token = strtok(message, CRLF);

    /* iterate through each token in message */
    while(token != NULL) {
        ret = strcmp(token, getter);

        /* if we found Host: we copy the URL to url */
        if(ret == 0) {
            token = strtok(NULL,CRLF);
            strcpy(url,token);
        }
        token = strtok(NULL, CRLF);
    }
    return url;
}
char *getCookieValue(char message[]){
    const char CRLF[5] = " =\r\n";
    const char getter[14] = "Cookie:";
    char *token;
    int ret;

    char *url = malloc(sizeof(char) * 20);

    token =strtok(message, CRLF);

    /* iterate through each token in message */
    while(token != NULL) {
        ret = strcmp(token, getter);

        /* if we found Host: we copy the URL to url */
        if(ret == 0) {
            token = strtok(NULL,CRLF);
            token = strtok(NULL,CRLF);
            strcpy(url,token);
        }
        token = strtok(NULL, CRLF);
    }
    return url;
}

char *getRequestUrl(char message[])
{
	const char CRLF[5] = " \r\n";
	char *token;

	char *reqUrl = malloc (sizeof (char) * 2000);

	token = strtok(message, CRLF);
	token = strtok(NULL, CRLF);

	strcat(reqUrl, token);

	return reqUrl; 
}


char *getColor(char message[]) {
	const char CRLF[7] = " /?=";
	const char getter[6] = "color";
	char *token;
	int ret;

	/* New pointer for returnValue*/
    char *color = malloc (sizeof (char) * 200);

    /* Get the first token */
    token = strtok(message, CRLF);
    while(token != NULL) {
    	ret = strcmp(token, getter);
    	if(ret == 0) {
    		token = strtok(NULL,CRLF);
    		token = strtok(NULL,CRLF);
    		strcpy(color, token);
    	}
    	token = strtok(NULL, CRLF);
    }
    return color;
}

char *getPayload(char message[]) {
    char CRLF[5] = " \r\n\r\n";
    const char getter[14] = "Content-Type:";
    char *token;
    int ret;

    /* New pointer for returnValue*/
    char *payload = malloc (sizeof (char) *2000);

    /* Get the first token */
    token = strtok(message, CRLF);

    /* iterate through each token in message */
    while(token != NULL) {
        ret = strcmp(token, getter);

        if (ret == 0) {
            token = strtok(NULL, CRLF);
            token = strtok(NULL, CRLF);
            
            while(token != NULL) {
                strcat(payload, token);
                strcat(payload, " ");
                token = strtok(NULL, " ");
            }
        }
        token = strtok(NULL, CRLF);
    }
    return payload;
}

void getwebsite(GString * html, char payload[]) {
    g_string_append(html,"<!DOCTYPE html> \r\n");
    g_string_append(html,"<html lang=\"en\"> \r\n");
    g_string_append(html,"<head> \r\n");
    g_string_append(html,"<meta charset=\"UTF-8\"> \r\n");
    g_string_append(html,"<title>Document</title> \r\n");
    g_string_append(html,"</head> \r\n");
    g_string_append(html,"<body> \r\n");
    g_string_append(html,"<p> This is a nice webpage. </p>");
    g_string_append(html, payload);
    g_string_append(html, "\r\n");
    g_string_append(html,"</body> \r\n");
    g_string_append(html,"</html> \r\n");
}
void getColorWebsite(GString * html, char colorPayload[]) {
    g_string_append(html,"<!DOCTYPE html> \r\n");
    g_string_append(html,"<html lang=\"en\"> \r\n");
    g_string_append(html,"<head> \r\n");
    g_string_append(html,"<meta charset=\"UTF-8\"> \r\n");
    g_string_append(html,"<title>Document</title> \r\n");
    g_string_append(html,"</head> \r\n");
    g_string_append(html,"<body style=background-color:");
    g_string_append(html, colorPayload);
    g_string_append(html, "> \r\n");
    g_string_append(html, "\r\n");
    g_string_append(html,"</body> \r\n");
    g_string_append(html,"</html> \r\n");
}